# Zudoku

![Demo](demo.gif)

Sudoku on the command line written in [Zig](https://ziglang.org) for ANSI
terminals.

Requires `stty` to enter raw mode and `tput` to hide the cursor.

## Controls

| Key(s)         | Action                     |
| -------------- | -------------------------- |
| h, j, k, l     | Move left, down, up, right |
| 1-9            | Insert                     |
| 0, Space, x, d | Remove                     |
| g, K           | Move to top                |
| G, J           | Move to bottom             |
| ^, H           | Move to far left           |
| $, L           | Move to far right          |

## Installation

[Download from SourceForge](https://sourceforge.net/projects/zig-sudoku/files)
or install with a supported package manager.

### [Yay](https://github.com/Jguer/yay)

```sh
yay -Sy zudoku
```

### [Osoy](https://gitlab.com/osoy/osoy)

```sh
osoy clone gitlab.com/rasmusmerzin/zudoku
osoy make zudoku
osoy link zudoku
```

## Build locally

```sh
zig build run
```
