const std = @import("std");
const eql = std.mem.eql;
const expect = std.testing.expect;
usingnamespace @import("list.zig");

test "construct" {
    const str = Str(8).empty();
    try expect(str.len == 0);
    try expect(str.cap == 8);
}

test "push" {
    var str = Str(8).empty();
    str.push('h');
    try expect(str.len == 1);
    try expect(str.items[0] == 'h');
}

test "slice" {
    var str = Str(8).empty();
    try expect(str.eql(""));
    try expect(str.slice().len == 0);
    str.push('s');
    str.push('t');
    str.push('r');
    try expect(str.len == 3);
    try expect(str.slice().len == 3);
    try expect(str.eql("str"));
}

test "subslice" {
    var str = Str(8).from("text");
    try expect(Str(2).from("ex").eql(str.subslice(1, 2)));
    try expect(Str(2).from("t").eql(str.subslice(3, 2)));
    try expect(Str(2).from("").eql(str.subslice(5, 2)));
}

test "append" {
    var str = Str(8).empty();
    str.append("str");
    try expect(str.len == 3);
    str.append(str.slice());
    try expect(str.len == 6);
    try expect(str.eql("strstr"));
}

test "from" {
    var str = Str(8).from("some");
    try expect(str.len == 4);
    try expect(str.eql("some"));
    str.append(str.slice());
    try expect(str.len == 8);
    try expect(str.eql("somesome"));
}

test "clear" {
    var str = Str(8).from("some");
    try expect(str.len == 4);
    try expect(str.eql("some"));
    str.clear();
    try expect(str.len == 0);
    try expect(str.eql(""));
}

test "push oob" {
    var str = Str(5).from("some");
    try expect(str.len == 4);
    str.push('!');
    try expect(str.len == 5);
    str.push('?');
    try expect(str.len == 5);
    try expect(str.eql("some!"));
}

test "copy" {
    var str1 = Str(4).from("some");
    var str2 = str1;
    str2.items[0] = 'S';
    try expect(!eql(u8, str1.slice(), str2.slice()));
    try expect(std.mem.eql(u8, str1.slice(), "some"));
    try expect(std.mem.eql(u8, str2.slice(), "Some"));
}

test "remove" {
    var str = Str(8).from("somwe");
    str.remove(3, 1);
    try expect(str.eql("some"));
    str.remove(4, 1);
    try expect(str.eql("some"));
    str.remove(0, 2);
    try expect(str.eql("me"));
}

test "take" {
    var str = Str(7).from("texting");
    try expect(str.take(1, 2).eql("ex"));
    try expect(str.eql("tting"));
    try expect(str.take(4, 2).eql("g"));
    try expect(str.eql("ttin"));
    try expect(str.pop().? == 'n');
    try expect(str.eql("tti"));
    try expect(str.popAt(1).? == 't');
    try expect(str.eql("ti"));
    str.clear();
    try expect(str.pop() == null);
    try expect(str.popAt(1) == null);
}

test "insert" {
    var str = Str(16).from("inrt");
    str.insert(2, "se");
    try expect(str.eql("insert"));
    str.insert(6, "?!");
    try expect(str.eql("insert?!"));
    str.insert(9, "?!");
    try expect(str.eql("insert?!"));
    str.insert(0, "?!");
    try expect(str.eql("?!insert?!"));
}

test "insert oob" {
    var str = Str(4).from("0123");
    str.insert(4, "23");
    try expect(str.eql("0123"));
    str.insert(0, "23");
    try expect(str.eql("2301"));
    str.insert(3, "23");
    try expect(str.eql("2302"));
}

test "seq" {
    try expect(eql(usize, &seq(3), &.{ 0, 1, 2 }));
}
