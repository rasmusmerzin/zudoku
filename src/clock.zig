pub fn milliString(milli: usize) [12]u8 {
    var res = [1]u8{':'} ** 12;

    res[11] = @truncate(u8, 48 + milli % 10);
    res[10] = @truncate(u8, 48 + milli % 100 / 10);
    res[9] = @truncate(u8, 48 + milli % 1000 / 100);
    res[8] = '.';

    const s = milli / 1000;
    res[7] = @truncate(u8, 48 + s % 10);
    res[6] = @truncate(u8, 48 + s % 60 / 10);

    const m = s / 60;
    res[4] = @truncate(u8, 48 + m % 10);
    res[3] = @truncate(u8, 48 + m % 60 / 10);

    var h = m / 60;
    if (h > 99) h = 99;
    res[1] = @truncate(u8, 48 + h % 10);
    res[0] = @truncate(u8, 48 + h % 60 / 10);

    return res;
}
