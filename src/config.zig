usingnamespace @import("std");
const NextError = process.ArgIterator.NextError;
usingnamespace @import("alloc.zig");
usingnamespace @import("list.zig");

const HELP_MSG = "USAGE: {s} [ah] [empty=45]\n";

pub const Config = struct {
    available: bool = false,
    empty: usize = 45,

    pub fn from_args() !Config {
        var iter = process.args();
        var exe: ?[]u8 = null;
        var args = List([]u8, 2).empty();
        var config = Config{};
        while (true) {
            const arg = try iter.next(&gpa.allocator) orelse break;
            if (exe) |_| args.push(arg) else exe = arg;
            if (args.len >= 2) break;
        }
        defer {
            gpa.allocator.free(exe.?);
            for (args.slice()) |arg| gpa.allocator.free(arg);
        }
        if (args.len == 0) {
            return config;
        }
        for (args.items[0]) |flag| {
            switch (flag) {
                'a' => config.available = true,
                'h' => {
                    debug.print(HELP_MSG, .{exe});
                    process.exit(0);
                },
                else => {},
            }
        }
        config.empty = fmt.parseInt(usize, args.last().?, 10) catch 45;
        debug.print("empty: {}\n", .{config.empty});
        return config;
    }
};
