const std = @import("std");
const mem = std.mem;
usingnamespace std.rand;
usingnamespace std.math;

pub fn List(comptime T: type, comptime cap: comptime_int) type {
    return struct {
        const Self = @This();

        cap: usize = cap,
        len: usize = 0,
        items: [cap]T = [1]T{undefined} ** cap,

        pub fn empty() Self {
            return Self{};
        }

        pub fn clear(self: *Self) void {
            self.len = 0;
        }

        pub fn from(items: []const T) Self {
            var self = Self{};
            self.append(items);
            return self;
        }

        pub fn slice(self: *const Self) []const T {
            return self.items[0..self.len];
        }

        pub fn subslice(self: *const Self, index: usize, count: usize) []const T {
            const start = min(self.len, index);
            var end = min(self.len, index + count);
            return self.items[start..end];
        }

        pub fn eql(self: *const Self, items: []const T) bool {
            return mem.eql(T, self.slice(), items);
        }

        pub fn push(self: *Self, item: T) void {
            if (self.len >= cap) return;
            self.items[self.len] = item;
            self.len += 1;
        }

        pub fn append(self: *Self, items: []const T) void {
            for (items) |item| self.push(item);
        }

        pub fn remove(self: *Self, index: usize, count: usize) void {
            var i = index + count;
            while (i < self.len) {
                self.items[i - count] = self.items[i];
                i += 1;
            }
            self.len = max(index, self.len - count);
        }

        pub fn take(self: *Self, index: usize, count: usize) Self {
            var taken = Self.from(self.subslice(index, count));
            self.remove(index, count);
            return taken;
        }

        pub fn popAt(self: *Self, index: usize) ?T {
            if (index >= self.len) return null;
            const item = self.items[index];
            var i = index + 1;
            while (i < self.len) {
                self.items[i - 1] = self.items[i];
                i += 1;
            }
            self.len -= 1;
            return item;
        }

        pub fn pop(self: *Self) ?T {
            if (self.len == 0) return null;
            self.len -= 1;
            return self.items[self.len];
        }

        pub fn last(self: *Self) ?T {
            if (self.len == 0) return null;
            return self.items[self.len - 1];
        }

        pub fn insert(self: *Self, index: usize, items: []const T) void {
            if (index > self.len or index >= cap) return;
            if (self.len > 0) {
                var i = self.len - 1;
                while (i >= index) {
                    const target = i + items.len;
                    if (target < cap) self.items[target] = self.items[i];
                    if (i == 0) break;
                    i -= 1;
                }
            }
            for (items) |item, local_i| {
                const i = index + local_i;
                if (i >= cap) return;
                self.items[i] = item;
            }
            self.len = min(cap, self.len + items.len);
        }

        pub fn randomize(self: *Self, random: *Random) void {
            var copied = Self.from(self.slice());
            var i: usize = 0;
            while (i < self.len) {
                const take_i = random.uintLessThan(usize, copied.len);
                self.items[i] = copied.popAt(take_i).?;
                i += 1;
            }
        }
    };
}

pub fn Str(comptime cap: comptime_int) type {
    return List(u8, cap);
}

pub fn seq(comptime count: comptime_int) [count]usize {
    var res = [1]usize{0} ** count;
    var i: usize = 0;
    while (i < count) {
        res[i] = i;
        i += 1;
    }
    return res;
}
