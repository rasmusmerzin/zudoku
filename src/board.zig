const std = @import("std");
const debug = std.debug;
const rng = @import("rng.zig");
const tty = @import("tty.zig");
usingnamespace @import("list.zig");
usingnamespace @import("neighbors.zig");

fn pntEq(lhs: ?[2]usize, rhs: ?[2]usize) bool {
    if (lhs == null and rhs == null) return true;
    if (lhs != null and rhs != null) {
        return lhs.?[0] == rhs.?[0] and lhs.?[1] == rhs.?[1];
    }
    return false;
}

pub const Board = struct {
    values: [9][9]u4 = [1][9]u4{[1]u4{0} ** 9} ** 9,
    locked: [9][9]bool = [1][9]bool{[1]bool{false} ** 9} ** 9,
    collisions: [9][9]bool = [1][9]bool{[1]bool{false} ** 9} ** 9,
    not_filled: usize = 81,
    hl: ?[2]usize = null,

    pub fn generate(empty_count: usize) Board {
        var board = Board{};
        board.not_filled = empty_count;
        if (empty_count >= 81) return board;
        const success = board.recGenerate(0);
        board.locked = [1][9]bool{[1]bool{true} ** 9} ** 9;
        var slots = List(usize, 81).from(&seq(81));
        var i: usize = 0;
        while (i < empty_count) {
            const take_i = rng.get().uintLessThan(usize, slots.len);
            const pos_i = slots.popAt(take_i).?;
            const x = pos_i / 9;
            const y = pos_i % 9;
            board.values[x][y] = 0;
            board.locked[x][y] = false;
            i += 1;
        }
        return board;
    }

    pub fn set(self: *Board, coord: [2]usize, value: u4) void {
        const previous_value = self.values[coord[0]][coord[1]];
        if (previous_value == 0 and value != 0) {
            self.not_filled -= 1;
        } else if (previous_value != 0 and value == 0) {
            self.not_filled += 1;
        }
        tty.printAt(.{ 0, 13 }, "\x1b[K");
        std.debug.print("Remaining: {}", .{self.not_filled});
        self.values[coord[0]][coord[1]] = value;
        self.collisions[coord[0]][coord[1]] = collisionAt(&self.values, coord);
        self.printAt(coord);
        for (neighbors(coord)) |neighbor| {
            const collision = collisionAt(&self.values, neighbor);
            self.collisions[neighbor[0]][neighbor[1]] = collision;
            self.printAt(neighbor);
        }
    }

    pub fn noCollisions(self: *const Board) bool {
        for (self.collisions) |column| {
            for (column) |collision| {
                if (collision) return false;
            }
        }
        return true;
    }

    pub fn highlight(self: *Board, new: ?[2]usize) void {
        const was = self.hl;
        self.hl = new;
        if (was) |c| self.printAt(c);
        if (new) |c| self.printAt(c);
    }

    pub fn printFull(self: *const Board) void {
        var i: usize = 0;
        while (i < 9 * 9) {
            self.printAt(.{ i % 9, i / 9 });
            i += 1;
        }
    }

    fn getRow(self: *const Board, y: usize) [9]u4 {
        var row = [1]u4{0} ** 9;
        var x: usize = 0;
        while (x < 9) {
            row[x] = self.values[x][y];
            x += 1;
        }
        return row;
    }

    fn printAt(self: *const Board, coord: [2]usize) void {
        const value = self.values[coord[0]][coord[1]];
        var str = Str(32).empty();
        if (pntEq(self.hl, coord)) str.append("\x1b[7m");
        const locked = self.locked[coord[0]][coord[1]];
        if (locked or value == 0 and !pntEq(self.hl, coord)) {
            str.append("\x1b[2m");
        }
        if (self.collisions[coord[0]][coord[1]]) {
            if (self.locked[coord[0]][coord[1]]) {
                str.append("\x1b[93m");
            } else str.append("\x1b[31m");
        }
        str.push(' ');
        if (value > 0) {
            str.append(&.{@intCast(u8, value) + 48});
        } else str.push('.');
        str.append("\x1b[m");
        tty.printAt(tty.coordToScreen(coord), str.slice());
    }

    fn recGenerate(self: *Board, index: usize) bool {
        if (index >= 9 * 9) return true;
        const x = index / 9;
        const y = index % 9;
        var available = availableAt(&self.values, .{ x, y });
        available.randomize(rng.get());
        if (available.len == 0) return false;
        for (available.slice()) |value| {
            self.values[x][y] = value;
            if (self.recGenerate(index + 1)) return true;
        }
        self.values[x][y] = 0;
        return false;
    }
};
