usingnamespace @import("game.zig");
usingnamespace @import("config.zig");

pub fn main() !void {
    const config = try Config.from_args();
    var game = Game.from_config(&config);
    try game.init();
    try game.loop();
}
