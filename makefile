release:
	@zig build -Drelease-small=true
	@mv -f zig-out/bin/zudoku zudoku
	@strip zudoku

run:
	@zig build run

test:
	@find src -type f | grep '\.\(test\|spec\)\.zig$$' | while read -r spec; do \
		echo "$$spec"; \
		zig test "$$spec" --cache-dir zig-cache || exit $?; \
	done

fmt:
	@zig fmt $$(find src -type f | grep '\.zig$$')

clean:
	@rm -rf zig-out zig-cache zudoku
