const std = @import("std");
const getStdIn = std.io.getStdIn;
const exit = std.process.exit;
const milliTimestamp = std.time.milliTimestamp;
const print = std.debug.print;
usingnamespace @import("clock.zig");
usingnamespace @import("tty.zig");
usingnamespace @import("board.zig");
usingnamespace @import("neighbors.zig");
usingnamespace @import("config.zig");

pub const Game = struct {
    board: Board,
    coord: [2]usize = [2]usize{ 0, 0 },
    start: i64 = undefined,
    print_available: bool = false,

    pub fn from_config(config: *const Config) Game {
        return Game{
            .board = Board.generate(config.empty),
            .print_available = config.available,
        };
    }

    pub fn init(self: *Game) !void {
        try setRaw();
        try hideCursor();
        clear();
        self.board.printFull();
        self.board.highlight(self.coord);
        self.start = milliTimestamp();
    }

    pub fn close(self: *Game) !void {
        try unsetRaw();
        try showCursor();
        self.board.highlight(null);
        printAt(.{ 0, 14 }, "\x1b[K");
        exit(0);
    }

    pub fn loop(self: *Game) !void {
        const stdin = getStdIn().reader();
        while (true) {
            try self.act(try stdin.readByte());
            try self.checkSolved();
        }
    }

    fn checkSolved(self: *Game) !void {
        if (self.board.not_filled == 0 and self.board.noCollisions()) {
            printAt(.{ 0, 12 }, "\x1b[J\n");
            const ms = @bitCast(u64, milliTimestamp() - self.start);
            print("Solved in {s}", .{milliString(@truncate(usize, ms))});
            try self.close();
        }
    }

    fn printAvailable(self: *const Game) void {
        const available = availableAt(&self.board.values, self.coord);
        printAt(.{ 0, 12 }, "\x1b[K");
        if (available.len == 0 or self.atLocked()) return;
        std.debug.print("Available:", .{});
        for (available.slice()) |v| std.debug.print(" {}", .{v});
    }

    fn printCoord(self: *Game) void {
        self.board.highlight(self.coord);
        if (self.print_available) self.printAvailable();
    }

    fn atLocked(self: *const Game) bool {
        return self.board.locked[self.coord[0]][self.coord[1]];
    }

    fn act(self: *Game, byte: u8) !void {
        switch (byte) {
            3 => try self.close(),

            104 => {
                if (self.coord[0] > 0) self.coord[0] -= 1;
                self.printCoord();
            },
            106 => {
                if (self.coord[1] < 8) self.coord[1] += 1;
                self.printCoord();
            },
            107 => {
                if (self.coord[1] > 0) self.coord[1] -= 1;
                self.printCoord();
            },
            108 => {
                if (self.coord[0] < 8) self.coord[0] += 1;
                self.printCoord();
            },

            103, 75 => {
                self.coord[1] = 0;
                self.printCoord();
            },
            71, 74 => {
                self.coord[1] = 8;
                self.printCoord();
            },
            94, 72 => {
                self.coord[0] = 0;
                self.printCoord();
            },
            36, 76 => {
                self.coord[0] = 8;
                self.printCoord();
            },
            122 => {
                self.coord = .{ 4, 4 };
                self.printCoord();
            },

            49...57 => {
                if (self.atLocked()) return;
                self.board.set(self.coord, @truncate(u4, byte - 48));
            },

            48, 32, 100, 120 => {
                if (self.atLocked()) return;
                self.board.set(self.coord, 0);
            },

            else => {},
        }
    }
};
