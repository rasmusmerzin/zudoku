const std = @import("std");
const rand = std.rand;
const time = std.time;

var rng: ?rand.Xoroshiro128 = null;

pub fn get() *rand.Random {
    if (rng == null) {
        const seed = @divTrunc(@bitCast(u128, time.nanoTimestamp()), 1000);
        rng = rand.DefaultPrng.init(@truncate(u64, seed));
    }
    return &rng.?.random;
}
