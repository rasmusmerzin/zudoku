usingnamespace @import("list.zig");

pub fn neighbors(coord: [2]usize) [20][2]usize {
    var res = [1][2]usize{undefined} ** 20;
    var i: usize = 0;
    var x: usize = 0;
    var y: usize = 0;
    while (x < 9) {
        if (x == coord[0] / 3 * 3) x += 3;
        if (x >= 9) break;
        res[i] = .{ x, coord[1] };
        x += 1;
        i += 1;
    }
    while (y < 9) {
        if (y == coord[1] / 3 * 3) y += 3;
        if (y >= 9) break;
        res[i] = .{ coord[0], y };
        y += 1;
        i += 1;
    }
    var box_i: usize = 0;
    while (box_i < 9) {
        const box_x = box_i / 3 + coord[0] / 3 * 3;
        const box_y = box_i % 3 + coord[1] / 3 * 3;
        if (box_x != coord[0] or box_y != coord[1]) {
            res[i] = .{ box_x, box_y };
            i += 1;
        }
        box_i += 1;
    }
    return res;
}

pub fn availableAt(values: *const [9][9]u4, coord: [2]usize) List(u4, 9) {
    var available = [1]bool{true} ** 9;
    for (neighbors(coord)) |neighbor| {
        const value = values[neighbor[0]][neighbor[1]];
        if (value != 0) available[value - 1] = false;
    }
    var list = List(u4, 9).empty();
    for (available) |v, index| {
        if (v) list.push(@truncate(u4, index + 1));
    }
    return list;
}

pub fn collisionAt(values: *const [9][9]u4, coord: [2]usize) bool {
    const value = values[coord[0]][coord[1]];
    if (value == 0) return false;
    for (neighbors(coord)) |neighbor| {
        const neighbor_value = values[neighbor[0]][neighbor[1]];
        if (value == neighbor_value) return true;
    }
    return false;
}
