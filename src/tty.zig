const print = @import("std").debug.print;
const cmd = @import("cmd.zig");

pub fn setRaw() !void {
    try cmd.call(&.{ "stty", "raw", "-echo" });
}

pub fn unsetRaw() !void {
    try cmd.call(&.{ "stty", "-raw", "echo" });
}

pub fn hideCursor() !void {
    try cmd.call(&.{ "tput", "civis" });
}

pub fn showCursor() !void {
    try cmd.call(&.{ "tput", "cnorm" });
}

pub fn coordToScreen(coord: [2]usize) [2]usize {
    var res = [2]usize{ coord[0], coord[1] };
    res[0] += @as(usize, @boolToInt(res[0] > 2)) + @boolToInt(res[0] > 5);
    res[1] += @as(usize, @boolToInt(res[1] > 2)) + @boolToInt(res[1] > 5);
    return res;
}

pub fn clear() void {
    print("\x1b[2J\x1b[H", .{});
}

pub fn printAt(pos: [2]usize, content: []const u8) void {
    print("\x1b[{};{}H{s}", .{ 1 + pos[1], 1 + pos[0] * 2, content });
}
