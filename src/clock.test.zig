const std = @import("std");
const eql = std.mem.eql;
const expect = std.testing.expect;
usingnamespace @import("clock.zig");

test "milliString" {
    try expect(eql(u8, &milliString(61941123), "17:12:21.123"));
}
