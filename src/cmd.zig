usingnamespace @import("std");
usingnamespace @import("alloc.zig");

pub fn call(argv: []const []const u8) !void {
    const proc = try ChildProcess.init(argv, &gpa.allocator);
    const r = try proc.spawnAndWait();
    proc.deinit();
}
