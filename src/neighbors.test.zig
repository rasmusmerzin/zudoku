const std = @import("std");
const eql = std.mem.eql;
const expect = std.testing.expect;
usingnamespace @import("neighbors.zig");

test "neighbors" {
    var res = neighbors(.{ 4, 4 });

    try expect(eql(usize, &res[0], &.{ 0, 4 }));
    try expect(eql(usize, &res[1], &.{ 1, 4 }));
    try expect(eql(usize, &res[2], &.{ 2, 4 }));
    try expect(eql(usize, &res[3], &.{ 6, 4 }));
    try expect(eql(usize, &res[4], &.{ 7, 4 }));
    try expect(eql(usize, &res[5], &.{ 8, 4 }));

    try expect(eql(usize, &res[6], &.{ 4, 0 }));
    try expect(eql(usize, &res[7], &.{ 4, 1 }));
    try expect(eql(usize, &res[8], &.{ 4, 2 }));
    try expect(eql(usize, &res[9], &.{ 4, 6 }));
    try expect(eql(usize, &res[10], &.{ 4, 7 }));
    try expect(eql(usize, &res[11], &.{ 4, 8 }));

    try expect(eql(usize, &res[12], &.{ 3, 3 }));
    try expect(eql(usize, &res[13], &.{ 3, 4 }));
    try expect(eql(usize, &res[14], &.{ 3, 5 }));

    try expect(eql(usize, &res[15], &.{ 4, 3 }));
    try expect(eql(usize, &res[16], &.{ 4, 5 }));

    try expect(eql(usize, &res[17], &.{ 5, 3 }));
    try expect(eql(usize, &res[18], &.{ 5, 4 }));
    try expect(eql(usize, &res[19], &.{ 5, 5 }));

    res = neighbors(.{ 4, 5 });
    try expect(eql(usize, &res[15], &.{ 4, 3 }));
    try expect(eql(usize, &res[16], &.{ 4, 4 }));

    res = neighbors(.{ 0, 5 });
    try expect(eql(usize, &res[12], &.{ 0, 3 }));
    try expect(eql(usize, &res[13], &.{ 0, 4 }));
    try expect(eql(usize, &res[14], &.{ 1, 3 }));
    try expect(eql(usize, &res[19], &.{ 2, 5 }));

    res = neighbors(.{ 0, 0 });
    try expect(eql(usize, &res[12], &.{ 0, 1 }));
    try expect(eql(usize, &res[19], &.{ 2, 2 }));

    res = neighbors(.{ 8, 8 });
    try expect(eql(usize, &res[12], &.{ 6, 6 }));
    try expect(eql(usize, &res[19], &.{ 8, 7 }));
}

const values = [9][9]u4{
    [1]u4{0} ** 9,
    [1]u4{0} ** 9,
    [9]u4{ 0, 0, 0, 0, 6, 0, 0, 0, 0 },
    [9]u4{ 0, 0, 0, 9, 2, 5, 0, 0, 0 },
    [9]u4{ 0, 8, 0, 0, 1, 4, 0, 8, 0 },
    [9]u4{ 0, 0, 0, 1, 0, 7, 0, 0, 0 },
    [1]u4{0} ** 9,
    [1]u4{0} ** 9,
    [1]u4{0} ** 9,
};

test "availableAt" {
    var available = availableAt(&values, .{ 4, 4 });
    try expect(available.len == 1);
    try expect(available.items[0] == 3);

    available = availableAt(&values, .{ 4, 5 });
    try expect(available.len == 3);
    try expect(available.items[0] == 3);
    try expect(available.items[1] == 4);
    try expect(available.items[2] == 6);

    available = availableAt(&values, .{ 5, 4 });
    try expect(available.len == 2);
    try expect(available.items[0] == 3);
    try expect(available.items[1] == 8);
}

test "collisionAt" {
    try expect(!collisionAt(&values, .{ 3, 3 }));
    try expect(!collisionAt(&values, .{ 4, 3 }));
    try expect(!collisionAt(&values, .{ 5, 4 }));
    try expect(collisionAt(&values, .{ 4, 1 }));
    try expect(collisionAt(&values, .{ 4, 7 }));
    try expect(collisionAt(&values, .{ 4, 4 }));
    try expect(collisionAt(&values, .{ 5, 3 }));
}
